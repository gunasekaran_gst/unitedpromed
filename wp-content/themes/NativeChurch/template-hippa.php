<?php
/*
  Template Name: Hippa
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
?>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12  our_company">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <h1><a href="#">Our Company</a></h1>
                    </div>
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        UnitedProMed Solutions is a leading provider of Revenue Cycle Management Services to the Medical
                        Billing Companies in the United States.</p>
                    <p>
                        UnitedProMed Solutions provides its customers a 24 x 7 support with effective delivery of
                        services. The infrastructure backbone is built on high-speed, redundant, reliable voice and data
                        networks that ride on best-in-class technologies. UnitedProMed Solutions's highly skilled
                        technology support teams ensure that the infrastructure is available round the clock.
                        UnitedProMed Solutions can support diverse operations and can be scaled up quickly to meet
                        customer’s increasing business needs. UnitedProMed Solutions leverages strong relationships with
                        leading technology vendors to access and acquire the latest technologies.
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padd_top_20 our_company_para">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        We are known for our Knowledge, Loyalty, Quality and the Commitment to enhance our client’s
                        medical billing experience.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid about_bg">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 specialties_have">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_30 padd_left_right_none">
                    <h1>Specialties we handle</h1>
                    <span class="divider"></span>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_20 sevices_list">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20">
                        <ul>
                            <li class="simple"><i class="fa fa-bolt"></i>Urgent Care</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Family/General Practice</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Chiropractic</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Physical Therapy</li>
                            <li class="simple"><i class="fa fa-bolt"></i>ENT</li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20">
                        <ul>
                            <li class="simple"><i class="fa fa-bolt"></i>Mental Health</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Behavioral Health</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Autism ABA Therapy</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Ophthalmology</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Anesthesiology</li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20">
                        <ul>
                            <li class="simple"><i class="fa fa-bolt"></i>Plastic Surgery</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Neurology</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Pain Management</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Wound Care</li>
                            <li class="simple"><i class="fa fa-bolt"></i>Hospitalist</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid billing_partner">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20 our_company left">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing_partner padd_top_30">
                    <ul>
                        <li class="simple">
                            <a href="http://unitedpromed.com/about-us/">Overview</a>
                        </li>
                        <li class="simple">
                            <a href="http://unitedpromed.com/infrastructure/">Infrastructure</a>
                        </li>
                        <li class="active">
                            <a href="http://unitedpromed.com/hippa/">HIPAA</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 padd_top_20 our_company">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Mission and Vision</p>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Our mission</p>
                </div>
                <p>
                    The mission of UnitedProMed Solutions is to provide superior solutions for healthcare providers,
                    enabling them to maximize their financial resources and improve the quality and availability of care
                    for their patients.
                </p>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Our values</p>
                </div>
                <p>
                    Integrity,Innovation and Results, three words are at the core of every decision we make and every
                    action we take. At UnitedProMed Solutions, we strive to maintain a culture where integrity is not
                    just a word but a way of life, where innovation is recognized and rewarded, and where results are
                    the natural by product of everything we do.
                </p>


                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Our pact</p>
                </div>
                <p>
                    At UnitedProMed Solutions, we pact with each of our clients to conduct our business with the highest
                    levels of professionalism, accountability, compliance and teamwork.
                </p>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Professionalism</p>
                </div>
                <p>
                    We expect all UnitedProMed Solutions team members to conduct themselves in a respectful, competent
                    and dependable manner, or in a word, as professionals.
                </p>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Compliance</p>
                </div>
                <p>
                    We expect to be held accountable, both as an organization and also as individuals. We demand the
                    same level of accountability from our business partners and associates.
                </p>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission_vission mission_vission_head padd_left_right_none">
                    <p class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        Teamwork</p>
                </div>
                <p>
                    UnitedProMed Solutions is only working as a team, both internally as well as with our clients, that
                    we can consistently accomplish the results we desire.
                </p>

            </div>
        </div>
    </div>

<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<?php
get_footer();
?>