<?php
global $imic_options;
$show_on_front = get_option('show_on_front');
 if ((!is_front_page()) || $show_on_front == 'posts'||(!is_page_template('template-home.php')&&!is_page_template('template-h-second.php')&&!is_page_template('template-h-third.php')&&!is_page_template('template-home-pb.php'))) {
     echo '</div></div>';
}
?>
<?php $options = get_option('imic_options'); ?>
<!-- Start Footer -->
<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
<footer class="site-footer">
    <div class="container">
        <div class="row">
        	<?php dynamic_sidebar('footer-sidebar'); ?>
        </div>
    </div>
</footer>
<?php endif; ?>
<footer class="site-footer-bottom">
    <div class="container">
        <div class="row">
            <?php
            if (!empty($options['footer_copyright_text'])) {
                echo '<div class="copyrights-col-left col-md-6 col-sm-6">'; ?>
                <p><?php _e('&copy; ','framework'); echo date('Y '); echo 'Unitedpro'; _e('. ','framework'); echo $options['footer_copyright_text']; ?></p>
                <?php echo '</div>';
            }
            ?>
            <div class="copyrights-col-right col-md-6 col-sm-6">
                <div class="social-icons">
                    <?php
                    $socialSites = $imic_options['footer_social_links'];
					foreach($socialSites as $key => $value) {
						if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        echo '<a href="mailto:' . $value . '"><i class="fa ' . $key . '"></i></a>';
						}
						elseif (filter_var($value, FILTER_VALIDATE_URL)) {
							echo '<a href="' . $value . '" target="_blank"><i class="fa ' . $key . '"></i></a>';
						}
						elseif($key == 'fa-skype' && $value != '') {
							echo '<a href="skype:' . $value . '?call"><i class="fa ' . $key . '"></i></a>';
						}
					}
                    ?>
                  </div>
            </div>
        </div>
    </div>
</footer>
<?php if ($options['enable_backtotop'] == 1) { 
echo'<a id="back-to-top"><i class="fa fa-angle-double-up"></i></a>';
 } ?>
</div>
<!-- End Boxed Body -->
<!-- LIGHTBOX INIT -->
<?php			
	if(isset($imic_options['switch_lightbox']) && $imic_options['switch_lightbox']== 0){?>
		<script>
			jQuery(document).ready(function() {
               jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
				  opacity: <?php if(isset($imic_options['prettyphoto_opacity']) && $imic_options['prettyphoto_opacity']!= ""){ echo $imic_options['prettyphoto_opacity']; } ?>,
				  social_tools: "",
				  deeplinking: false,
				  allow_resize: <?php if(isset($imic_options['prettyphoto_opt_resize']) && $imic_options['prettyphoto_opt_resize']!= ""){ echo $imic_options['prettyphoto_opt_resize']; } else { echo 'true'; } ?>,
				  show_title: <?php if(isset($imic_options['prettyphoto_title']) && $imic_options['prettyphoto_title']== 0){ echo 'true'; } else echo 'false'; ?>,
				  theme: '<?php if(isset($imic_options['prettyphoto_theme']) && $imic_options['prettyphoto_theme']!= ""){ echo $imic_options['prettyphoto_theme']; } ?>',
				});
				/*jQuery('.sort-source a').click(function(){
					var sortval = jQuery(this).parent().attr('data-option-value');
					jQuery(".sort-destination li a").removeAttr('data-rel');
    				jQuery(".sort-destination li a").attr('data-rel', "prettyPhoto["+sortval+"]");
				});*/
            });
		</script>
	<?php }elseif(isset($imic_options['switch_lightbox']) && $imic_options['switch_lightbox']== 1){ ?>
    	<script>
			jQuery(document).ready(function() {
				jQuery('.format-gallery').each(function(){
					jQuery(this).magnificPopup({
  						delegate: 'a.magnific-gallery-image', // child items selector, by clicking on it popup will open
  						type: 'image',
						gallery:{enabled:true}
  						// other options
					});
				});
				jQuery('.magnific-image').magnificPopup({ 
  					type: 'image'
					// other options
				});
				jQuery('.magnific-video').magnificPopup({ 
  					type: 'iframe'
					// other options
				});
				jQuery('.title-subtitle-holder-inner').magnificPopup({
  					delegate: 'a.magnific-video', // child items selector, by clicking on it popup will open
  					type: 'iframe',
					gallery:{enabled:true}
  				// other options
				});
			});
		</script>
	<?php }
	
	// Google events link target 
   $event_google_open_link = isset($imic_options['event_google_open_link'])?$imic_options['event_google_open_link']:0;
   if($event_google_open_link == 1)
   { ?>
	 <script>
	 jQuery(document).ready(function(){
		jQuery('a[href^="https://www.google.com/calendar"]').attr('target','_blank');
	});
	</script>
   <?php } ?>
<?php wp_footer(); ?>
 <?php $SpaceBeforeBody = $imic_options['space-before-body'];
    echo $SpaceBeforeBody;
 ?>

<script>
    jQuery(document).ready(function(){
        jQuery(".menu_parent").click(function(e){
            e.preventDefault();
            jQuery(this).parent().children('.sub-menu').slideToggle();
        });
    });
</script>
</body>
</html>