<?php
/*
  Template Name:Mental
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
?>

<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_top_20 our_company left">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing_partner">
                    <ul>
                        <li class="active">
                            <a href="http://unitedpromed.com/services/">Services Overview</a>
                        </li>
                        <li class="simple">
                            <a href="http://unitedpromed.com/medical-billing/">Medical Billing</a>
                        </li>
                        <li class="simple">
                            <a href="http://unitedpromed.com/mental-health-billing/">Mental Health Billing</a>
                        </li>
                        <ul class="nav navbar-nav">
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/adobe') }}">Medical Coding <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menu">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Coding Services </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> Work Flow Diagram </a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/adobe') }}">Medical Billing <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menus">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Billing Services </a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/adobe') }}">Customized Billing <i class="icon-arrow-right"></i></a>
                                    <ul class="dropdown-menu  sub-menu">
                                        <li><a href="{{ url('/expresiencedesign') }}"> Eligibility Verifications </a></li>
                                        <li><a href="{{ url('/after_effects') }}">  Charge Entry  </a></li>
                                        <li><a href="{{ url('/expresiencedesign') }}"> Payment Posting </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> AR Management  </a></li>
                                        <li><a href="{{ url('/expresiencedesign') }}"> Denial Management </a></li>
                                        <li><a href="{{ url('/after_effects') }}"> Old AR Recovery </a></li>
                                    </ul>
                                </li>
                            </ul>
                        </ul>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 padd_top_20 our_company_para">
                <div class="block_title"><h1>Mental Health Billing</h1></div>
                <p class="para_text">We understand that with Mental Health Billing comes additional third party
                    carriers, like MHCA, American Behavioral, UBH and Value Options to name a few. Our billing experts
                    specialize in mental health billing. We work well with these carriers and will handle all billing
                    and claims processing functions for you, so that you can spend time providing the best care to your
                    patients. By partnering with us, you get the experience and manpower you need to gain control of
                    your insurance billing and finances along with a dedicated, professional team that works effectively
                    with you and your office staff.
                <h3 style="font-size: 20px;color: #ed2f42;text-align: left;font-family:Roboto;font-weight:500;font-style:normal"
                    class="vc_custom_heading">Service Includes:</h3>
                <span class="vc_empty_space_inner"></span>
                <div class="wpb_content_element list only_icon" style="">
                    <ul>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Create new patient accounts
                            from
                            face sheets and copies of insurance cards
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Enter charges
                            from your super bills (mailed, delivered, faxed or uploaded to Unitedpro)
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Send monthly patient
                            statements
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post payments received from
                            patient statements
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;File electronic and hardcopy
                            insurance claims
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Re-file rejected claims with
                            corrections
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Post insurance
                            ERA/EOB/REMIT (mailed, delivered, faxed or uploaded to Unitedpro)
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Secondary insurance billing
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Automated appointment
                            reminders
                            via text and email
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Appointment scheduler</li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Month end reporting</li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Provider credentialing
                            assistance
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Review past-due accounts and
                            more…
                        </li
                    </ul>
                    <br>
                    <h3 style="font-size: 20px;color: #ed2f42;text-align: left;font-family:Roboto; font-weight:500;font-style:normal"
                        class="vc_custom_heading">Additional Services:</h3>
                    <span class="vc_empty_space_inner"></span></div>
                <div class="wpb_content_element list only_icon" style="">
                    <ul>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Integrated EHR
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;On-site training
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;Practice consulting
                        </li>
                        <li class="simple para_texts"><i class="fa fa-bolt sim"></i> &nbsp;New practice set-up
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
</div>


<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>
<?php
get_footer();
?>






